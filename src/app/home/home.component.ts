import { Component, OnDestroy, OnInit } from "@angular/core";
import Client from "fhirclient/lib/Client";

import * as FHIR from "fhirclient";
import { KeycloakAuthService } from "../services/login/KeycloakAuth.service";
import { DataCommunicationService } from "../services/data-communication/data-communication.service";
import { EhrLauncherConfig } from "../interface/ehr-launcher-config.interface";
import { EhrConfigurationService } from "../services/EhrConfiguration.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit, OnDestroy {
  constructor(
    private _keycloakAuthService: KeycloakAuthService,
    private _dataCommunicationService: DataCommunicationService,
    private _ehrConfigurationService: EhrConfigurationService
  ) {}

  async ngOnInit(): Promise<void> {
    await this._keycloakAuthService.init();

    await this.initFHIRCommunication();
  }

  ngOnDestroy(): void {}

  loginKeycloak(): void {
    console.log("logging to keycloak");
    this._keycloakAuthService.login().catch(console.error);
  }

  logoutKeycloak(): void {
    console.log("logout from keycloak");
    this._keycloakAuthService
      .logout()
      .then(() => {
        sessionStorage.clear();
      })
      .catch(console.error);
  }

  private async initFHIRCommunication(): Promise<void> {
    try {
      const client: Client = await FHIR.oauth2.ready();
      if (!client.state.tokenResponse) {
        console.error("No token response from FHIR");
        return;
      }

      const configuration: EhrLauncherConfig | undefined =
        this._ehrConfigurationService.getConfiguration(client.state.serverUrl);

      console.log("using token: ", client.state.tokenResponse.access_token);

      const resp: boolean = await this.verifyFHIRAccessToken(client);
      if (!resp) {
        return;
      }
      this.processClientData(client);
    } catch (err: any) {
      console.error(err);
    }
  }

  /**
   * verify with data communication backend service if the provided client's
   * acess token is valid
   */
  private async verifyFHIRAccessToken(client: Client): Promise<boolean> {
    try {
      const resp: any = await this._dataCommunicationService
        .verifyAccessToken(client.state.tokenResponse!!.access_token!!)
        .toPromise();

      // if the response includes the token field then the check has passed
      return resp["token"] !== null;
    } catch (err: any) {
      console.error(err);
      return false;
    }
  }

  private processClientData(client: Client) {
    console.log("GOT CLIENT: ", client);
    console.log(JSON.stringify(client.getIdToken()));
  }
}
