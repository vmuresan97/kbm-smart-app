/**
 * Use this instead of hard coding the routes
 * in our project.
 *
 * Eg: when wanting to redirect the user to a certain route
 */
export enum POSSIBLE_ROUTES {
  LAUNCHER = "/launcher",
  HOME = "/home",
}
