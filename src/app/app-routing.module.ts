import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EhrLauncherComponent } from "./ehr-launcher/ehr-launcher.component";
import { HomeComponent } from "./home/home.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { AuthGuard } from "./services/app.guard";
import { TestComponent } from "./test/test.component";

const routes: Routes = [
  { path: "", redirectTo: "/test", pathMatch: "full" },
  {
    path: "home",
    component: HomeComponent,
  },
  { path: "test", component: TestComponent },
  { path: "launcher", component: EhrLauncherComponent },

  { path: "**", component: PageNotFoundComponent }, // IMPORTANT: needs to be the last!
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  // providers: [AuthGuard],
})
export class AppRoutingModule {}
