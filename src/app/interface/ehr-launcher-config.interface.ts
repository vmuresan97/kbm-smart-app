/**
 * After launching the EHR service we get all needed details
 */
export interface EhrLauncherConfig {
  /**
   * client id given by the EHR upon registering this frontend to it!
   */
  clientId: string;
  scope: string;
  redirectUri: string;
}
