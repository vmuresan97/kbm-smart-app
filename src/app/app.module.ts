import { APP_INITIALIZER, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EhrLauncherComponent } from "./ehr-launcher/ehr-launcher.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { HomeComponent } from "./home/home.component";
import { environment } from "src/environments/environment";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import { TestComponent } from "./test/test.component";
import { HttpClientModule } from "@angular/common/http";

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: environment.keycloakURL,
        realm: environment.keycloakReal,
        clientId: environment.keycloakClientId,
      },
      initOptions: {
        onLoad: "check-sso",
        silentCheckSsoRedirectUri:
          window.location.origin + "/assets/silent-check-sso.html",
        checkLoginIframe: true,
        checkLoginIframeInterval: 25,
        enableLogging: true,
      },
      bearerExcludedUrls: ["/assets", "/clients/public"],
    });
}

@NgModule({
  declarations: [
    AppComponent,
    EhrLauncherComponent,
    PageNotFoundComponent,
    HomeComponent,
    TestComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    KeycloakAngularModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
