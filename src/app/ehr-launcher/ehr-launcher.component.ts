import { ThrowStmt } from "@angular/compiler";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import * as FHIR from "fhirclient";
import { POSSIBLE_ROUTES } from "../enum/possible-routes.enum";
import { EhrLauncherConfig } from "../interface/ehr-launcher-config.interface";
import { DataCommunicationService } from "../services/data-communication/data-communication.service";
import { EhrConfigurationService } from "../services/EhrConfiguration.service";

@Component({
  selector: "app-ehr-launcher",
  templateUrl: "./ehr-launcher.component.html",
  styleUrls: ["./ehr-launcher.component.scss"],
})
export class EhrLauncherComponent implements OnInit {
  private configuration: EhrLauncherConfig | null = null;

  constructor(private _ehrConfigurationService: EhrConfigurationService) {}

  async ngOnInit(): Promise<void> {
    await this.readConfigurationAndAuthorize();
  }

  private async readConfigurationAndAuthorize(): Promise<void> {
    //TODO: check vendor here
    this.configuration = {
      clientId: "4cd66a66-9ca0-456a-bc22-554024ff9747",
      scope: "launch openid fhirUser patient/*.read offline_access",
      redirectUri: POSSIBLE_ROUTES.HOME,
    };

    //TODO: rn this is hardcoded because this component can be open by any vendor, maybe make a component per vendor to solve this?
    this._ehrConfigurationService.setConfiguration(
      "https://api.logicahealth.org/TestSandboxVMuresan/data",
      this.configuration
    );

    await FHIR.oauth2.authorize({
      clientId: this.configuration.clientId,
      scope: [this.configuration.scope].join(" "),
      redirectUri: this.configuration.redirectUri,
    });
  }
}
