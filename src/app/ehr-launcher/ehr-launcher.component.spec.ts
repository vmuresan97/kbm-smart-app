import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EhrLauncherComponent } from './ehr-launcher.component';

describe('EhrLauncherComponent', () => {
  let component: EhrLauncherComponent;
  let fixture: ComponentFixture<EhrLauncherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EhrLauncherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EhrLauncherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
