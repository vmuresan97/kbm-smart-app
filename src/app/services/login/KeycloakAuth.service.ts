import { Injectable } from "@angular/core";
import { KeycloakService } from "keycloak-angular";
import { KeycloakProfile } from "keycloak-js";

/**
 * Authentification service based on keycloak
 */
@Injectable({
  providedIn: "root",
})
export class KeycloakAuthService {
  private isLoggedIn = false;
  private userProfile: KeycloakProfile | null = null;

  constructor(private readonly keycloak: KeycloakService) {}

  public async init() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    if (this.isLoggedIn) {
      this.userProfile = await this.keycloak.loadUserProfile();
    }
  }

  public async login() {
    return await this.keycloak.login();
  }

  public async logout() {
    return await this.keycloak.logout();
  }

  public getUserProfile(): KeycloakProfile | null {
    return this.userProfile;
  }

  public getIsLoggedIn(): boolean {
    return this.isLoggedIn;
  }
}
