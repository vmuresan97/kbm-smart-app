import { Injectable } from "@angular/core";
import { EhrLauncherConfig } from "../interface/ehr-launcher-config.interface";

@Injectable({
  providedIn: "root",
})
export class EhrConfigurationService {
  private readonly configuration: Map<string, EhrLauncherConfig> = new Map<
    string,
    EhrLauncherConfig
  >();

  /**
   * Set the configuration for a specific EHR vendor after a successful login
   */
  public setConfiguration(serverUrl: string, config: EhrLauncherConfig): void {
    this.configuration.set(serverUrl, config);
  }

  public getConfiguration(serverUrl: string): EhrLauncherConfig | undefined {
    return this.configuration.get(serverUrl);
  }
}
