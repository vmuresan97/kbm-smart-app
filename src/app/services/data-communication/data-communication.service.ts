import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";

/**
 * This service is responsable with dealing with the data communication microservice
 */
@Injectable({
  providedIn: "root",
})
export class DataCommunicationService {
  private readonly apiURL: string = environment.dataCommunicationServiceURL;

  constructor(private _http: HttpClient) {}

  public verifyAccessToken(token: string): Observable<string> {
    return this._http
      .get<string>(`${this.apiURL}/verifyAccessToken/${token}`)
      .pipe(tap(console.log), catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    console.error(err);
    return throwError(`An error occured - Error code ${err.status}`);
  }
}
