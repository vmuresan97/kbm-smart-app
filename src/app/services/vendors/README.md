# This folder contains the services which handle the different implementation of the SMART on FHIR protocol

Possible vendors:

- SMART
- OpenEpic
- Allscripts
- Cerner
- Health Intersections
- HSPC

The specified vendors can have different API implementation, we have to take note of that.
