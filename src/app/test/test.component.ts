import { Component, OnInit } from "@angular/core";
import { KeycloakAuthService } from "../services/login/KeycloakAuth.service";

@Component({
  selector: "app-test",
  templateUrl: "./test.component.html",
  styleUrls: ["./test.component.scss"],
})
export class TestComponent implements OnInit {
  constructor(private _keycloakService: KeycloakAuthService) {}

  ngOnInit(): void {}

  logoutKeycloak(): void {
    console.warn("logout from keycloak");
    this._keycloakService.logout().catch(console.error);
  }
}
